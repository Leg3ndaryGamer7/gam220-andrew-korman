# include <iostream>
using namespace std;

int main(int argc, char* argv[])
{
	int array[] = { 90, 93, 82, 68, 100, 54, 101, 84, 93, 103, 77, 76, 93, 85, 70 }; //sets up the array
	int sum = 0; //these variables will be used later
	int maximum = array[0];
	int minimum = array[0];

	for (int i = 0; i < 15; ++i) //adds all numbers in the array then prints the sum
	{
		sum += array[i];
	}
	cout << "The sum of the numbers is " << sum << endl;

	float average = sum / 15.0f; //finds the average of the array
	cout << "The average of the numbers is " << average << endl;

	for (int i = 0; i < 15; ++i) //prints out the first number in the array that's less than 70
	{
		if (array[i] < 70)
		{
			cout << "The first number less than 70 is " << array[i] << endl;
			break;
		}
	}

	for (int i = 0; i < 15; ++i) //prints out any number higher than 100
	{
		if (array[i] > 100)
		{
			cout << array[i] << " and ";
		}
	}
	cout << "are greater than 100" << endl;

	for (int i = 0; i < 15; ++i) //finds the smallest and largets numbers in the array and prints them
	{
		if (array[i] < minimum)
		{
			minimum = array[i];
		}
		else if (array[i] > maximum)
		{
			maximum = array[i];
		}
	}

	//cout << "The largest number is " << maximum << " and the smallest number is " << minimum << endl;
	printf("The largest number is %d and the smallest number is %d\n", maximum, minimum);

	cin.get();

	return 0;
}