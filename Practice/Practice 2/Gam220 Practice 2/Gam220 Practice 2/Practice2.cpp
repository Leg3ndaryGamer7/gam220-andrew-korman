#include <iostream>
#include <String>
#include <cctype>
using namespace std;

int main(int argc, char* argv[])
{
	/*What happened was char a = cin.get(); is getting the first character in the string I type in. char b = cin.get(); is doing the same thing as char a except this time it is getting the 2nd character of the string I type in. 
	after that std::getline(cin, myString); is setting myString equal the rest of the string I type in starting at the character directly after what char b was set to in the previous line of code.
	after that we have 2 cout lines which are printing char a & char b on the first cout line and the 2nd cout line is printing myString.*/
	cout << "I Love Anime/Manga and Light Novel's." << endl;
	cout << "> ";
	char a = cin.get();
	char b = cin.get();
	string myString;
	std::getline(cin, myString);
	cout << "a: " << a << ", b: " << b << endl;
	cout << "myString: " << myString << endl;


	string currentWord;
	string names;
	string words[26] = { "Apple", "Beta", "Cool", "Demon", "Evermore", "Final", "God", "Halo", "In", "Jobs", "Kill", "Lemon", "Money", "Noob", "Of", "Pills", "Quest", "Ready", "Start", "Time", "Up", "Versus", "Win", "Xbox", "Your", "Zealot"};

	cout << "Enter a name: " << endl;
	std::getline(cin, names);
	for (unsigned int i = 0; i < names.length(); ++i) {
		names[i] = tolower(names[i]);
	}


	for (int i = 0; i < names.length(); ++i)
	{
		if (names[i] == 'a')
		{
			currentWord = words[0];
		}
		else if (names[i] == 'b')
		{
			currentWord = words[1];
		}
		else if (names[i] == 'c')
		{
			currentWord = words[2];
		}
		else if (names[i] == 'd')
		{
			currentWord = words[3];
		}
		else if (names[i] == 'e')
		{
			currentWord = words[4];
		}
		else if (names[i] == 'f')
		{
			currentWord = words[5];
		}
		else if (names[i] == 'g')
		{
			currentWord = words[6];
		}
		else if (names[i] == 'h')
		{
			currentWord = words[7];
		}
		else if (names[i] == 'i')
		{
			currentWord = words[8];
		}
		else if (names[i] == 'j')
		{
			currentWord = words[9];
		}
		else if (names[i] == 'k')
		{
			currentWord = words[10];
		}
		else if (names[i] == 'l')
		{
			currentWord = words[11];
		}
		else if (names[i] == 'm')
		{
			currentWord = words[12];
		}
		else if (names[i] == 'n')
		{
			currentWord = words[13];
		}
		else if (names[i] == 'o')
		{
			currentWord = words[14];
		}
		else if (names[i] == 'p')
		{
			currentWord = words[15];
		}
		else if (names[i] == 'q')
		{
			currentWord = words[16];
		}
		else if (names[i] == 'r')
		{
			currentWord = words[17];
		}
		else if (names[i] == 's')
		{
			currentWord = words[18];
		}
		else if (names[i] == 't')
		{
			currentWord = words[19];
		}
		else if (names[i] == 'u')
		{
			currentWord = words[20];
		}
		else if (names[i] == 'v')
		{
			currentWord = words[21];
		}
		else if (names[i] == 'w')
		{
			currentWord = words[22];
		}
		else if (names[i] == 'x')
		{
			currentWord = words[23];
		}
		else if (names[i] == 'y')
		{
			currentWord = words[24];
		}
		else if (names[i] == 'z')
		{
			currentWord = words[25];
		}
		cout << names[i] << " is for " << currentWord << endl;
	}

	/*for (int i = 0; i < 3; ++i)
	{
		cout << char('a' + i) << " is for " << names[i] << endl;
	}

	for (char c = 'a'; c <= 'c'; ++c)
	{
		cout << c << " is for " << names[c - 97] << endl;

	}

	cout << "Hello!" << endl;

	cin.get();*/

	return 0;
}