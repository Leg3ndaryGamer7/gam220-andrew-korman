#include "Blob.h"


Blob::Blob()
{
	radius = 100.0f;

	image = nullptr;
}

Blob::Blob(GPU_Image* texture)
{
	radius = 100.0f;

	image = texture;
}

Blob::Blob(GPU_Image* texture, float newX, float newY)
{
	position.x = newX;
	position.y = newY;

	radius = 100.0f;

	image = texture;
}

void Blob::Update(float dt)
{
	// Update position based on velocity
	position += velocity * dt;

	if (position.x < 0)
	{
		position.x = 0;
		velocity.x = -velocity.x;
	}
	if (position.y < 0)
	{
		position.y = 0;
		velocity.y = -velocity.y;
	}
	if (position.x > 800)
	{
		position.x = 800;
		velocity.x = -velocity.x;
	}
	if (position.y > 600)
	{
		position.y = 0;
		velocity.y = -velocity.y;
	}
}

bool Blob::DoesCollide(Blob* other)
{
	float dx = other->position.x - position.x;
	float dy = other->position.y - position.y;
	float distance = dx*dx + dy * dy;
	return (distance < radius + other->radius);
}

void Blob::Grow(float addSize)
{
	radius += addSize / 2;
}

void Blob::Draw(GPU_Target* screen)
{
	// Draw the image on the screen at the right size (2*radius across)
	GPU_BlitScale(image, nullptr, screen, position.x, position.y, radius / image->w, radius / image->h);
}

void Blob::Draw(GPU_Target* screen, float playerRadius)
{
	// Color enemies according to the player's size
	if (radius < playerRadius)
		GPU_SetRGB(image, 150, 255, 150);
	else
		GPU_SetRGB(image, 255, 150, 150);

	// Draw the image on the screen at the right size (2*radius across)
	GPU_BlitScale(image, nullptr, screen, position.x, position.y, radius / image->w, radius / image->h);

	GPU_UnsetColor(image);
}