#pragma once

#include "SDL_gpu.h"
#include "Vec2.h"

class Blob
{
public:
	Vec2 position;
	Vec2 velocity;
	float radius;

	GPU_Image* image;

	Blob();
	Blob(GPU_Image* texture);
	Blob(GPU_Image* texture, float newX, float newY);

	void Update(float dt);

	bool DoesCollide(Blob* other);
	void Grow(float addSize);

	void Draw(GPU_Target* screen);
	void Draw(GPU_Target* screen, float playerRadius);
};