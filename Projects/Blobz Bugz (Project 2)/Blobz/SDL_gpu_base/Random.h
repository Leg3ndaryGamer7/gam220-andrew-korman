#pragma once


#include <random>

class Random
{
public:
	Random()
		: generator(source())
	{}

	int Range(int low, int high)
	{
		std::uniform_int_distribution<int> distribution(low, high);
		return distribution(generator);
	}

	float Range(float low, float high)
	{
		std::uniform_real_distribution<float> distribution(low, high);
		return distribution(generator);
	}
private:

	std::random_device source;
	std::mt19937 generator;
};