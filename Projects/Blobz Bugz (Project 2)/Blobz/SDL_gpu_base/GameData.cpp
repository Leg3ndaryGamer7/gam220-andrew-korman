#include "GameData.h"
#include <stdexcept>


GameData::GameData()
{
	screen = GPU_Init(800, 600, GPU_DEFAULT_INIT_FLAGS);

	if (screen == nullptr)
		throw std::runtime_error("Could not initialize SDL_gpu!");


	smileImage = images.Load("smile.bmp");
	if (smileImage == nullptr)
		throw std::runtime_error("Failed to load images!");

	font.load("FreeSans.ttf", 16);

	startingRadius = 50;
	startingPosition = Vec2(screen->w / 2.0f, screen->h / 2.0f);

	player.image = smileImage;


	initialNumberOfEnemies = 10;
	gameOver = false;
	done = false;

	dt = 0.0f;
	frameStartTime = 0;
	frameEndTime = 0;


	keystates = SDL_GetKeyboardState(nullptr);
}

GameData::~GameData()
{

	// Clean up all of the memory we allocated

	for (unsigned int i = 0; i < enemies.size(); ++i)
	{
		delete enemies[i];
	}
	enemies.clear();

	images.Free();

	GPU_Quit();
}

void GameData::Restart()
{

}

void GameData::Run()
{
	dt = 0.0f;
	frameStartTime = SDL_GetTicks();

	done = false;

	Start();

	while (!done)
	{
		HandleEvents();
		HandleInputState();

		Update();
		Draw();

		EndFrame();
	}
}

void GameData::Start()
{

	gameOver = false;


	player.radius = startingRadius;
	player.position = startingPosition;

	for (unsigned int i = 0; i < initialNumberOfEnemies; ++i)
	{
		float randomX = rnd.Range(0, 800);
		float randomY = rnd.Range(0, 600);
		Blob* blob = new Blob(smileImage, randomX, randomY);
		blob->velocity.x = rnd.Range(-200.0f, 200.0f);
		blob->velocity.y = rnd.Range(-200.0f, 200.0f);

		// Start smaller than the player
		blob->radius = 2 + rnd.Range(1.0f, player.radius);
		// Make half of these guys bigger than the player
		if (i >= initialNumberOfEnemies / 2)
			blob->radius += player.radius;

		enemies.push_back(blob);
	}
}

void GameData::HandleEvents()
{
	SDL_Event event;
	while (SDL_PollEvent(&event))
	{
		if (event.type == SDL_QUIT)
			done = true;
		if (event.type == SDL_KEYDOWN)
		{
			if (event.key.keysym.sym == SDLK_ESCAPE)
				done = true;
			if (event.key.keysym.sym == SDLK_r)
			{
				gameOver = false;

				player.position = startingPosition;

				for (auto e = enemies.begin(); e != enemies.end(); )
				{
					delete *e;
				}
				enemies.clear();

				for (unsigned int i = 0; i < initialNumberOfEnemies; ++i)
				{
					float randomX = rnd.Range(0.0f, 800.0f);
					float randomY = rnd.Range(0.0f, 600.0f);
					Blob* blob = new Blob(smileImage, randomX, randomY);
					blob->velocity.x = rnd.Range(-200.0f, 200.0f);
					blob->velocity.y = rnd.Range(-200.0f, 200.0f);

					// Start smaller than the player
					blob->radius = 2 + rnd.Range(1.0f, player.radius);
					// Make half of these guys bigger than the player
					if (i >= initialNumberOfEnemies / 2)
						blob->radius += player.radius;

					enemies.push_back(blob);
				}
			}
		}
	}
}

void GameData::HandleInputState()
{

	// Player input
	if (keystates[SDL_SCANCODE_W])
		player.velocity.y = -100;
	else if (keystates[SDL_SCANCODE_S])
		player.velocity.y = 100;
	else
		player.velocity.y = 0;

	if (keystates[SDL_SCANCODE_A])
		player.velocity.x = -100;
	else if (keystates[SDL_SCANCODE_D])
		player.velocity.x = 0;
	else
		player.velocity.x = 0;
}

void GameData::Update()
{
	// Update the world
	player.Update(dt);


	for (unsigned int i = 0; i < enemies.size(); ++i)
	{
		enemies[i]->Update(dt);
	}

	if (!gameOver)
	{
		for (auto e = enemies.begin(); e != enemies.end();)
		{
			Blob* enemy = *e;
			if (player.DoesCollide(enemy))
			{
				if (player.radius >= enemy->radius)
				{
					player.Grow(enemy->radius);
					delete enemy;
					e = enemies.erase(e);

					if (enemies.size() == 0)
					{
						gameOverMessage = "You are big!";
					}
					continue;
				}
				else
				{
					gameOver = true;
					gameOverMessage = "You are a big loser.";
					break;
				}
			}

			++e;
		}
	}

}

void GameData::Draw()
{
	// Draw the world

	GPU_ClearRGBA(screen, 150, 200, 150, 255);

	if (!gameOver || enemies.size() == 0)
		player.Draw(screen);

	for (unsigned int i = 0; i < enemies.size(); ++i)
	{
		enemies[i]->Draw(screen, player.radius);
	}

	if (gameOver)
	{
		int w = font.getWidth("%s", gameOverMessage.c_str()) + 10;
		int h = font.getHeight("%s", gameOverMessage.c_str()) + 10;

		GPU_Rect box = { screen->w / 2.0f - w / 2.0f, screen->h / 2.0f - h / 2.0f, w, h };
		GPU_RectangleRoundFilled2(screen, box, 10, GPU_MakeColor(255, 255, 255, 100));
		GPU_RectangleRound2(screen, box, 10, GPU_MakeColor(0, 0, 0, 100));

		font.drawBox(screen, box, NFont::CENTER, "%s", gameOverMessage.c_str());
	}

}

void GameData::EndFrame()
{
	GPU_Flip(screen);
	// Frame is done rendering
	SDL_Delay(1);
	frameEndTime = SDL_GetTicks();
	dt = (frameEndTime - frameStartTime) / 1000.0f;
	frameStartTime = frameEndTime;
}