#pragma once

#include <string>
#include <vector>
#include "SDL_gpu.h"
#include "ImageManager.h"
#include "NFont_gpu.h"
#include "Random.h"
#include "Blob.h"

using namespace std;

class GameData
{
public:
	GPU_Target* screen;

	ImageManager images;
	NFont font;
	Random rnd;

	GPU_Image* smileImage;

	bool gameOver;
	string gameOverMessage;

	Blob player;
	vector<Blob*> enemies;

	float dt;
	bool done;


	GameData();
	~GameData();

	void Restart();
	void Run();

private:


	float startingRadius;
	Vec2 startingPosition;

	unsigned int initialNumberOfEnemies;
	const Uint8* keystates;

	Uint32 frameStartTime;
	Uint32 frameEndTime;

	void Start();

	void HandleEvents();
	void HandleInputState();

	void Update();
	void Draw();
	void EndFrame();

};