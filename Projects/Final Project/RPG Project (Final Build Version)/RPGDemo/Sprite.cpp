#include "Sprite.h"


Sprite::Sprite()
{
	spritesheet = nullptr;
	sourceRect = {0,0,0,0};
}

Sprite::Sprite(GPU_Image* sheet, GPU_Rect sourceRect)
{
	Set(sheet, sourceRect);
}

void Sprite::Set(GPU_Image* sheet, GPU_Rect sourceRect)
{
	spritesheet = sheet;
	this->sourceRect = sourceRect;
}

void Sprite::Draw(GPU_Target* screen, float x, float y) const
{
	GPU_Rect src = sourceRect;
	GPU_Blit(spritesheet, &src, screen, x, y);
}

void Sprite::DrawCell(GPU_Target* screen, int cellSize, const Vec2& offset, int x, int y) const
{
	GPU_Rect src = sourceRect;
	float destX = offset.x + cellSize * x + cellSize / 2;
	float destY = offset.y + cellSize * y + cellSize / 2;
	GPU_Blit(spritesheet, &src, screen, destX, destY);
}

void Sprite::DrawCellScale(GPU_Target* screen, int cellSize, const Vec2& offset, int x, int y, float scale) const
{
	GPU_Rect src = sourceRect;
	float destX = offset.x + cellSize * x + cellSize / 2;
	float destY = offset.y + cellSize * y + cellSize / 2;
	GPU_BlitScale(spritesheet, &src, screen, destX, destY, scale, scale);
}

void Sprite::DrawBounds(GPU_Target* screen, const GPU_Rect& bounds) const
{
	GPU_Rect src = sourceRect;
	GPU_Rect dest = bounds;
	GPU_BlitRect(spritesheet, &src, screen, &dest);
}
