#pragma once

#include <map>
#include <string>
#include "Character.h"
#include "Item.h"
#include <stdlib.h>
#include <time.h>
#include "HealingPotion.h"

using namespace std;

// Hero inherits from charecter, serves as the players stats and inventory.
class Hero : public Character{
public:
	Hero() {
		statsArrayPtr = nullptr;
		inventory = vector<Item*>();
	}
	
	void tryPickUp() {
		//if item at feet, pick it up
		//AddItem();
	}

	virtual void GenerateStats(int* targetArray, int size) {
		for (int i = 0; i < size; i++) {
			targetArray[i] = 10 + (rand() % 15);
		}
	}
	int* statsArrayPtr;

	void AddItem(Item *type) {
		inventory.push_back(type);
	}

	int UseItem(Item* emptyItem) {
		int returnVal = (*inventory.end())->Activate((*inventory.end())->tier);
		emptyItem = (*inventory.end());
		inventory.erase(inventory.end());
		return returnVal;
	}

	vector<Item*> inventory;

private:
	
	
	
};
