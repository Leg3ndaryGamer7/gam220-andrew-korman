#pragma once

#include <map>
#include <string>
#include "Item.h"
#include <stdlib.h>
#include <time.h>

using namespace std;

//
class HealingPotion : public Item {
public:
	HealingPotion():Item() {
		tier = 1 + (rand() % 5);
	}
	
	int Activate(int potionStrength) override{
		switch (potionStrength) {
		case 1:
			return (10 + (rand() % 5));
		case 2:
			return (25 + (rand() % 15));
		case 3:
			return (50 + (rand() % 35));
		case 4:
			return (100 + (rand() % 55));
		case 5:
			return (250 + (rand() % 75));
		case 6:
			return (500 + (rand() % 95));
		}	
	}
	
private:

};